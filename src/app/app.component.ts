import { Component, OnInit  } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title: String = 'Angular4-Echarts-Example';
  showloading: Boolean = true;
    constructor() {
      setTimeout(() => {
        this.showloading = false;
      }, 100);

    };
    chartOption = {
      title: {
        text: '堆叠区域图'
      },
      tooltip: {
        trigger: 'axis'
      },
      legend: {
        data: ['邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎']
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          boundaryGap: false,
          data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
        }
      ],
      yAxis: [
        {
          type: 'value'
        }
      ],
      series: [
        {
          name: '邮件营销',
          type: 'line',
          stack: '总量',
          areaStyle: { normal: {} },
          data: [120, 132, 101, 134, 90, 230, 210]
        },
        {
          name: '联盟广告',
          type: 'line',
          stack: '总量',
          areaStyle: { normal: {} },
          data: [220, 182, 191, 234, 290, 330, 310]
        },
        {
          name: '视频广告',
          type: 'line',
          stack: '总量',
          areaStyle: { normal: {} },
          data: [150, 232, 201, 154, 190, 330, 410]
        },
        {
          name: '直接访问',
          type: 'line',
          stack: '总量',
          areaStyle: { normal: {} },
          data: [320, 332, 301, 334, 390, 330, 320]
        },
        {
          name: '搜索引擎',
          type: 'line',
          stack: '总量',
          label: {
            normal: {
              show: true,
              position: 'top'
            }
          },
          areaStyle: { normal: {} },
          data: [820, 932, 901, 934, 1290, 1330, 1320]
        }
      ]
    };
    Baroptions = {
      tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
      },
      legend: {
        orient: 'vertical',
        x: 'left',
        data: ['直达', '营销广告', '搜索引擎', '邮件营销', '联盟广告', '视频广告', '百度', '谷歌', '必应', '其他']
      },
      series: [
        {
          name: '访问来源',
          type: 'pie',
          selectedMode: 'single',
          radius: [0, '30%'],

          label: {
            normal: {
              position: 'inner'
            }
          },
          labelLine: {
            normal: {
              show: false
            }
          },
          data: [
            { value: 335, name: '直达', selected: true },
            { value: 679, name: '营销广告' },
            { value: 1548, name: '搜索引擎' }
          ]
        },
        {
          name: '访问来源',
          type: 'pie',
          radius: ['40%', '55%'],

          data: [
            { value: 335, name: '直达' },
            { value: 310, name: '邮件营销' },
            { value: 234, name: '联盟广告' },
            { value: 135, name: '视频广告' },
            { value: 1048, name: '百度' },
            { value: 251, name: '谷歌' },
            { value: 147, name: '必应' },
            { value: 102, name: '其他' }
          ]
        }
      ]
    };
    linkoption =  {
      tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
          type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      legend: {
        data:['直接访问','邮件营销','联盟广告','视频广告','搜索引擎','百度','谷歌','必应','其他']
      },
      grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
      },
      xAxis : [
        {
          type : 'category',
          data : ['周一','周二','周三','周四','周五','周六','周日']
        }
      ],
      yAxis : [
        {
          type : 'value'
        }
      ],
      series : [
        {
          name:'直接访问',
          type:'bar',
          data:[320, 332, 301, 334, 390, 330, 320]
        },
        {
          name:'邮件营销',
          type:'bar',
          stack: '广告',
          data:[120, 132, 101, 134, 90, 230, 210]
        },
        {
          name:'联盟广告',
          type:'bar',
          stack: '广告',
          data:[220, 182, 191, 234, 290, 330, 310]
        },
        {
          name:'视频广告',
          type:'bar',
          stack: '广告',
          data:[150, 232, 201, 154, 190, 330, 410]
        },
        {
          name:'搜索引擎',
          type:'bar',
          data:[862, 1018, 964, 1026, 1679, 1600, 1570],
          markLine : {
            lineStyle: {
              normal: {
                type: 'dashed'
              }
            },
            data : [
              [{type : 'min'}, {type : 'max'}]
            ]
          }
        },
        {
          name:'百度',
          type:'bar',
          barWidth : 5,
          stack: '搜索引擎',
          data:[620, 732, 701, 734, 1090, 1130, 1120]
        },
        {
          name:'谷歌',
          type:'bar',
          stack: '搜索引擎',
          data:[120, 132, 101, 134, 290, 230, 220]
        },
        {
          name:'必应',
          type:'bar',
          stack: '搜索引擎',
          data:[60, 72, 71, 74, 190, 130, 110]
        },
        {
          name:'其他',
          type:'bar',
          stack: '搜索引擎',
          data:[62, 82, 91, 84, 109, 110, 120]
        }
      ]
    };
    datamapvalue = [
         {name: '海门', value: [121.15,31.89,9]},
         {name: '鄂尔多斯', value: [109.781327,39.608266,12]},
         {name: '招远', value: [120.38,37.35,12]},
         {name: '舟山', value: [122.207216,29.985295,12]},
         {name: '齐齐哈尔', value: [123.97,47.33,14]},
         {name: '盐城', value: [120.13,33.38,15]},
         {name: '赤峰', value: [118.87,42.28,16]},
         {name: '青岛', value: [120.33,36.07,18]},
         {name: '乳山', value: [121.52,36.89,18]},
         {name: '金昌', value: [102.188043,38.520089,19]}
     ];
    gaugeoption = {
      tooltip : {
        formatter: "{a} <br/>{c} {b}"
      },
      toolbox: {
        show: true,
        feature: {
          restore: {show: true},
          saveAsImage: {show: true}
        }
      },
      series : [
        {
          name: '速度',
          type: 'gauge',
          z: 3,
          min: 0,
          max: 220,
          splitNumber: 11,
          radius: '50%',
          axisLine: {            // 坐标轴线
            lineStyle: {       // 属性lineStyle控制线条样式
              width: 10
            }
          },
          axisTick: {            // 坐标轴小标记
            length: 15,        // 属性length控制线长
            lineStyle: {       // 属性lineStyle控制线条样式
              color: 'auto'
            }
          },
          splitLine: {           // 分隔线
            length: 20,         // 属性length控制线长
            lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
              color: 'auto'
            }
          },
          axisLabel: {
            backgroundColor: 'auto',
            borderRadius: 2,
            color: '#eee',
            padding: 3,
            textShadowBlur: 2,
            textShadowOffsetX: 1,
            textShadowOffsetY: 1,
            textShadowColor: '#222'
          },
          title : {
            // 其余属性默认使用全局文本样式，详见TEXTSTYLE
            fontWeight: 'bolder',
            fontSize: 20,
            fontStyle: 'italic'
          },
          detail : {
            // 其余属性默认使用全局文本样式，详见TEXTSTYLE
            formatter: function (value) {
              value = (value + '').split('.');
              value.length < 2 && (value.push('00'));
              return ('00' + value[0]).slice(-2)
                + '.' + (value[1] + '00').slice(0, 2);
            },
            fontWeight: 'bolder',
            borderRadius: 3,
            backgroundColor: '#444',
            borderColor: '#aaa',
            shadowBlur: 5,
            shadowColor: '#333',
            shadowOffsetX: 0,
            shadowOffsetY: 3,
            borderWidth: 2,
            textBorderColor: '#000',
            textBorderWidth: 2,
            textShadowBlur: 2,
            textShadowColor: '#fff',
            textShadowOffsetX: 0,
            textShadowOffsetY: 0,
            fontFamily: 'Arial',
            width: 100,
            color: '#eee',
            rich: {}
          },
          data:[{value: 40, name: 'km/h'}]
        },
        {
          name: '转速',
          type: 'gauge',
          center: ['20%', '55%'],    // 默认全局居中
          radius: '35%',
          min:0,
          max:7,
          endAngle:45,
          splitNumber:7,
          axisLine: {            // 坐标轴线
            lineStyle: {       // 属性lineStyle控制线条样式
              width: 8
            }
          },
          axisTick: {            // 坐标轴小标记
            length:12,        // 属性length控制线长
            lineStyle: {       // 属性lineStyle控制线条样式
              color: 'auto'
            }
          },
          splitLine: {           // 分隔线
            length:20,         // 属性length控制线长
            lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
              color: 'auto'
            }
          },
          pointer: {
            width:5
          },
          title: {
            offsetCenter: [0, '-30%'],       // x, y，单位px
          },
          detail: {
            // 其余属性默认使用全局文本样式，详见TEXTSTYLE
            fontWeight: 'bolder'
          },
          data:[{value: 1.5, name: 'x1000 r/min'}]
        },
        {
          name: '油表',
          type: 'gauge',
          center: ['77%', '50%'],    // 默认全局居中
          radius: '25%',
          min: 0,
          max: 2,
          startAngle: 135,
          endAngle: 45,
          splitNumber: 2,
          axisLine: {            // 坐标轴线
            lineStyle: {       // 属性lineStyle控制线条样式
              width: 8
            }
          },
          axisTick: {            // 坐标轴小标记
            splitNumber: 5,
            length: 10,        // 属性length控制线长
            lineStyle: {        // 属性lineStyle控制线条样式
              color: 'auto'
            }
          },
          axisLabel: {
            formatter:function(v){
              switch (v + '') {
                case '0' : return 'E';
                case '1' : return 'Gas';
                case '2' : return 'F';
              }
            }
          },
          splitLine: {           // 分隔线
            length: 15,         // 属性length控制线长
            lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
              color: 'auto'
            }
          },
          pointer: {
            width:2
          },
          title : {
            show: false
          },
          detail : {
            show: false
          },
          data:[{value: 0.5, name: 'gas'}]
        },
        {
          name: '水表',
          type: 'gauge',
          center : ['77%', '50%'],    // 默认全局居中
          radius : '25%',
          min: 0,
          max: 2,
          startAngle: 315,
          endAngle: 225,
          splitNumber: 2,
          axisLine: {            // 坐标轴线
            lineStyle: {       // 属性lineStyle控制线条样式
              width: 8
            }
          },
          axisTick: {            // 坐标轴小标记
            show: false
          },
          axisLabel: {
            formatter:function(v){
              switch (v + '') {
                case '0' : return 'H';
                case '1' : return 'Water';
                case '2' : return 'C';
              }
            }
          },
          splitLine: {           // 分隔线
            length: 15,         // 属性length控制线长
            lineStyle: {       // 属性lineStyle（详见lineStyle）控制线条样式
              color: 'auto'
            }
          },
          pointer: {
            width:2
          },
          title: {
            show: false
          },
          detail: {
            show: false
          },
          data:[{value: 0.5, name: 'gas'}]
        }
      ]
    };
}
