# NgEchartsExample

>Angular(4)和百度echarts的整合demo

## 快速开始

```
npm install 
```
启动  
```
ng serve -o
```
## 示例

![](http://oqr3htxnb.bkt.clouddn.com/17-9-22/74142817.jpg)

## 鸣谢

- [echarts](https://github.com/ecomfe/echarts)
- [ngx-echarts](https://github.com/xieziyu/ngx-echarts.git)

